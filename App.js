const express = require('express');


const app = express();
const bodyParser = require('body-parser');
const port = 3000;





// app.get('/', (req, res) => {
//   res.send('Hello World!')
// })
app.use(bodyParser.json());
app.use('/', require('./routes'));
app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
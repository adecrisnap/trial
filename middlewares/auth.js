var auth = {
    setResponse: function(res,status,message) {
        res.status(status).json({
           "status": status,
           "message": message
        });
        return;
    },
}

module.exports = auth;

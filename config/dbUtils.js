const sql = require('mssql');
var dbSalesDelivery = require("../config/dbSalesDelivery.js");
const config = dbSalesDelivery.getDbConfig();

const pool = new sql.ConnectionPool(config)
  .connect()
  .then(pool => {
    console.log('Connected to MSSQL')
    return pool
  })
  .catch(err => console.log('Database Connection Failed! Bad Config: ', err))

module.exports = {
  sql, pool
}
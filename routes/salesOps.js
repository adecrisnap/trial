var dbSalesDelivery = require("../config/dbSalesDelivery.js");
var auth = require("../middlewares/auth.js");
var async = require("async");
const sql = require("mssql");
const constant = require("../middlewares/constant");
const config = dbSalesDelivery.getDbConfig();

var salesOps = {
  postLocationData: async function (req, res) {
    // console.log("masuk post location data");
    // console.log(req.body);
      
    const { data } = req.body;
    var localdate = new Date(data.timestamp);
    var servertime = new Date();
    //var serverDt = servertime.getYear() + "-" + servertime.getMonth() + "-" + servertime.getDate() + " " + servertime.getHours() + ":" + servertime.getMinutes() + ":" + servertime.getSeconds()
  
    //console.log(data.timestamp)
    // console.log('localdate : ', localdate)
    // console.log('servertime : ',servertime)
    // //console.log(serverDt)
    // console.log('servertime with time : ', new Date().getTime())

    // console.log(data.coords.latitude)
    // console.log(data.coords.longitude)
    // console.log(data.coords.accuracy)
    // console.log(data.STATE_INOUT)
    // console.log(data.location_id)
    // console.log(data.location_name)
    //console.log(currentDateSystem)
    
    var config = dbSalesDelivery.getDbConfig();
    const pool = new sql.ConnectionPool(config, function (err) {
      if (err) {
        console.log(err);
        pool.close();
        auth.setResponse(res, 400, err);
        return;
      } else {
        var request = new sql.Request(pool);
        var sqlx = "EXEC BprOps.dbo.proc_PostDataCheckInOutDriver " + 
        "'" +data.user_id + "'" + "," +
        "'" +data.location_id + "'" + "," +
        "'" +data.location_name + "'" + "," +

        "'" +localdate.toISOString() + "'" + "," +
        "'" +servertime.toISOString() + "'" + "," +
        
         +data.coords.latitude + "," +
         +data.coords.longitude  +  "," +
         +data.STATE_INOUT 

        console.log ('sql syntax ' , sqlx)

        request.query(sqlx, function (err, recordset) {
          if (err) {
            pool.close();
            auth.setResponse(res, 400, err);
            return;
          } else {
            pool.close();
            return res.status(201).json({ message: "OK CREATED" });
          }
        });
      }
    });
  },

  getScheduleDelivery: async function (req, res) {
    const pool = new sql.ConnectionPool(config, function (err) {
      if (err) {
        pool.close();
        auth.setResponse(res, 400, err);
        return;
      } else {
        var request = new sql.Request(pool);
        var user_id = req.query.nik;
        var delivery_date = req.query.date;
        var sqlx =
          "EXEC BprOps.dbo.proc_GetDeliveryByNikAndDate " +
          "'" +
          user_id +
          "'" +
          "," +
          "'" +
          delivery_date +
          "'";

        console.log(sqlx);
        request.query(sqlx, function (err, recordsetx) {
          if (err) {
            pool.close();
            auth.setResponse(res, 400, err);
            return;
          } else {
            pool.close();
            let data = recordsetx.recordsets[0];
            (hash = data.reduce(
              (p, c) => (
                p[c.ScheduleDetail_CustomerID]
                  ? p[c.ScheduleDetail_CustomerID].push(c)
                  : (p[c.ScheduleDetail_CustomerID] = [c]),
                p
              ),
              {}
            )),
              (newData = Object.keys(hash).map((k) => ({
                ScheduleDetail_CustomerID: k,
                details: hash[k],
              })));

            //console.log(newData)
            //return res.status(200).json(recordsetx.recordsets[0]);
            return res.status(200).json(newData);
          }
        });
      }
    });
  },

  getDOSummary: async function (req, res) {
    const pool = new sql.ConnectionPool(config, function (err) {
      if (err) {
        pool.close();
        auth.setResponse(res, 400, err);
        return;
      } else {
        var request = new sql.Request(pool);
        var datestart = req.query.datestart;
        var dateend = req.query.dateend;
        var sqlx =
          "EXEC BprOps.dbo.proc_DeliveryOrder_Summary " +
          "'" +
          datestart +
          "'" +
          "," +
          "'" +
          dateend +
          "'";

        console.log(sqlx);
        request.query(sqlx, function (err, recordsetx) {
          if (err) {
            pool.close();
            auth.setResponse(res, 400, err);
            return;
          } else {
            pool.close();
            return res.status(200).json(recordsetx.recordsets[0]);
          }
        });
      }
    });
  },

  getBprDepo: function (req, res) {
    var config = dbSalesDelivery.getDbConfig();
    const pool = new sql.ConnectionPool(config, function (err) {
      if (err) {
        pool.close();
        auth.setResponse(res, 400, err);
        return;
      } else {
        var request = new sql.Request(pool);
        var sqlx = "EXEC BprOps.dbo.proc_GetDepo";
        request.query(sqlx, function (err, recordsetx) {
          if (err) {
            pool.close();
            auth.setResponse(res, 400, err);
            return;
          } else {
            var jsonArr = [];
            async.eachSeries(
              recordsetx.recordsets[0],
              function (obj, callback) {
                var jsonRes = {
                  depoId: obj.Depo_ID,
                  depoName: obj.Depo_Name,
                };
                jsonArr.push(jsonRes);
                return callback();
              },
              function (err) {
                if (err) {
                  console.log(err);
                  pool.close();
                  auth.setResponse(res, 400, err);
                  return;
                } else {
                  pool.close();
                  return res.status(200).json(jsonArr);
                }
              }
            );
          }
        });
      }
    });
  },

  getBprlocation: function (req, res) {
    var config = dbSalesDelivery.getDbConfig();
    const pool = new sql.ConnectionPool(config, function (err) {
      if (err) {
        pool.close();
        auth.setResponse(res, 400, err);
        return;
      } else {
        var request = new sql.Request(pool);
        var sqlx = "EXEC BprOps.dbo.proc_Getlocation";
        request.query(sqlx, function (err, recordsetx) {
          if (err) {
            pool.close();
            auth.setResponse(res, 400, err);
            return;
          } else {
            var jsonArr = [];
            async.eachSeries(
              recordsetx.recordsets[0],
              function (obj, callback) {
                var jsonRes = {
                  locationId: obj.Adr_nr,
                  locationName: obj.adr_name,
                };
                jsonArr.push(jsonRes);
                return setImmediate(callback);
              },
              function (err) {
                if (err) {
                  console.log(err);
                  pool.close();
                  auth.setResponse(res, 400, err);
                  return;
                } else {
                  pool.close();
                  return res.status(200).json(jsonArr);
                }
              }
            );
          }
        });
      }
    });
  },

  getUserByPosst: function (req, res) {
    const { posst } = req.query;
    console.log(posst);

    var config = dbSalesDelivery.getDbConfig();
    const pool = new sql.ConnectionPool(config, function (err) {
      if (err) {
        pool.close();
        auth.setResponse(res, 400, err);
        return;
      } else {
        var request = new sql.Request(pool);
        request.input("pt", sql.VarChar, "PT Belitang Panen Raya%");
        var sqlx =
          "SELECT [nik], [nama], [gsber], [golongan], [lokasi], [email], [phone], [posst], [posisi], [dept], [divisi], [ho]" +
          " FROM [BprOps].[dbo].[vw_User]" +
          " WHERE is_active = 1 AND [gsber] LIKE @pt ";
        if (posst === constant.ICT_DIVISION) {
          request.input("sales", sql.VarChar, "SALES & MARKETING DEPARTMENT");
          request.input(
            "delivery",
            sql.VarChar,
            "OPERATION & LOGISTIC DEPARTMENT"
          );
          sqlx += "AND [dept] IN (@sales , @delivery)";
        }
        if (posst === constant.SALESMAN) {
          request.input("sales", sql.VarChar, "SALES & MARKETING DEPARTMENT");
          sqlx += "AND [dept] = @sales";
        }
        if (posst === constant.DELIVERY) {
          request.input(
            "delivery",
            sql.VarChar,
            "OPERATION & LOGISTIC DEPARTMENT"
          );
          sqlx += "AND [dept] = @delivery";
        }
        console.log(sqlx);
        request.query(sqlx, function (err, recordsetx) {
          if (err) {
            pool.close();
            auth.setResponse(res, 400, err);
            return;
          } else {
            var jsonArr = [];
            async.eachSeries(
              recordsetx.recordsets[0],
              function (obj, callback) {
                var jsonRes = {
                  userNik: obj.nik,
                  userName: obj.nama,
                  userGsber: obj.gsber,
                  userGolongan: obj.golongan,
                  userLokasi: obj.lokasi,
                  userEmail: obj.email,
                  userPhone: obj.phone,
                  userPosst: obj.posst,
                  userPosisi: obj.posisi,
                  userDept: obj.dept,
                  userDivisi: obj.divisi,
                  userHO: obj.ho === "y",
                };
                jsonArr.push(jsonRes);
                return callback();
              },
              function (err) {
                if (err) {
                  console.log(err);
                  pool.close();
                  auth.setResponse(res, 400, err);
                  return;
                } else {
                  pool.close();
                  return res.status(200).json(jsonArr);
                }
              }
            );
          }
        });
      }
    });
  },

  setUserDeactive: function (req, res) {
    console.log("masuk set deactive");
    const { nik, user } = req.body;
    console.log(nik);
    console.log(user);

    var config = dbSalesDelivery.getDbConfig();
    const pool = new sql.ConnectionPool(config, function (err) {
      if (err) {
        pool.close();
        auth.setResponse(res, 400, err);
        return;
      } else {
        var request = new sql.Request(pool);
        request.input("datetime", sql.DateTime, new Date());
        request.input("nik", sql.Int, nik);
        request.input("user", sql.Int, user);
        var sqlx =
          "update ReportTemplate.dbo.t_karyawan5 set is_active = 0, updated_at = @datetime, updated_by = @user  where nik=@nik";
        console.log(sqlx);
        request.query(sqlx, function (err, recordset) {
          if (err) {
            pool.close();
            auth.setResponse(res, 400, err);
            return;
          } else {
            pool.close();
            return res.status(200).json({ message: "ok" });
          }
        });
      }
    });
  },
};

module.exports = salesOps;

var express = require("express");
var fs = require('fs');
var router = express.Router();
const salesOps = require('./salesOps.js');
const mobileDelivery = require('./mobileDelivery.js');
const multer = require("multer");

const storage = multer.diskStorage({
  //destination: "/home/itsep/messaging_server/uploads/",
  destination: "D:/image/",
    filename: function (req, file, cb) {
      cb(null,  file.originalname);
    },
  });
  
  const diskStorage = multer({ storage: storage });

   //mobile
   router.get('/planner_sonumber_byplanid_and_customerid/:planid/:customerid', mobileDelivery.get_planner_sonumber_byplanid_and_customerid);
   router.get('/planner_soitem_bysonumber/:sonumber', mobileDelivery.get_planner_soitem_bysonumber);
   router.get('/planner_headerid_bydriverid_deliverytoday/:driverid', mobileDelivery.get_planner_headerid_bydriverid_deliverytoday);
   router.get('/planner_customerid_byplanid/:planid/:driverid', mobileDelivery.get_planner_customers_bydeliveryid);
   router.get('/planner_accumulated_soitem_byplanid_and_customerid/:ruteid/:customerid', mobileDelivery.get_planner_accumulated_soitem_byplanid_and_customerid);

   router.get('/status_cico_driver/:customerid/:driverid', mobileDelivery.get_status_cico_driver);
   router.get('/petty_cash_exist_customer/:customerlocation', mobileDelivery.get_petty_cash_exist_customer);
   router.get('/petty_cash_exist_daily/:vehicleruteid', mobileDelivery.get_petty_cash_exist_daily);
   router.get('/return_scheduled_notprocessed_bycustomerid/:customerid', mobileDelivery.get_return_scheduled_notprocessed_bycustomerid);
   router.get('/return_scheduledid_notprocessed_bycustomerid/:customerid', mobileDelivery.get_return_scheduledid_notprocessed_bycustomerid);
   router.get('/customers', mobileDelivery.get_customers);
   router.get('/products', mobileDelivery.get_products);  
   
   router.post('/post_checkinout', mobileDelivery.postLocationData2);
   router.post('/post_goods_receive', mobileDelivery.post_goods_receive);
   router.post('/post_petty_cash_with_rute', mobileDelivery.post_petty_cash_with_rute);
   router.post('/post_petty_cash_daily', mobileDelivery.post_petty_cash_daily);
   router.post('/post_return_nonscheduled', mobileDelivery.post_return_nonscheduled);
   router.post('/post_return_scheduled', mobileDelivery.post_return_scheduled);

   router.post("/upload", diskStorage.single("image"), async (req, res) => {
     //console.log(req);
   try {
   var jsonArr = [];
   var jsonRes = {
     path: req.file.path,
   };
   jsonArr.push(jsonRes);
     res.status(200).send(jsonArr);  
   } catch (error) {
     res.status(500).send("Error");
   }
 });

module.exports = router;

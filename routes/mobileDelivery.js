var dbSalesDelivery = require("../config/dbSalesDelivery.js");
var auth = require("../middlewares/auth.js");
var async = require("async");
//const sql = require("mssql");
const config = dbSalesDelivery.getDbConfig();

const { success, error, validation } = require("../responseApi.js");

const { pool, sql } = require("../config/dbUtils.js");

const generateRandomString = (myLength) => {
  const chars =
    "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";
  const randomArray = Array.from(
    { length: myLength },
    (v, k) => chars[Math.floor(Math.random() * chars.length)]
  );

  const randomString = randomArray.join("");
  return randomString;
};

var mobileDelivery = {
  get_return_scheduledid_notprocessed_bycustomerid:  async function (req, res) {
    const { customerid } = req.params;

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("customer_id", sql.VarChar, customerid)
          .execute("BprOps.dbo.proc_Planner_GetReturnIDScheduledNotPickedUp");
      })
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));
      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });

  },


  get_return_scheduled_notprocessed_bycustomerid:  async function (req, res) {
    const { customerid } = req.params;

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("customer_id", sql.VarChar, customerid)
          .execute("BprOps.dbo.proc_Planner_GetReturnScheduledNotPickedUp");
      })
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));
      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });

  },

  get_products:  async function (req, res) {
    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .execute("BprOps.dbo.proc_GetProducts");
      })
      .then((result) => {
        var jsonArr = [];
        async.eachSeries(
          result.recordsets[0],
          function (obj, callback) {
            var jsonRes = {
              product_code: obj.product_code,
              product_name: obj.product_name,
              product_variant: obj.product_variant,
              product_unit: obj.product_unit,
              packing_unit: obj.packing_unit,
            };
            jsonArr.push(jsonRes);
            return callback();
          },
           function (err) {
             if (err) {
               auth.setResponse(res, 400, err);
               return;
             } else {  
               return res.status(200).json(jsonArr);
             }
           }
        );

        // return res
        //       .status(200)
        //       .json(success("OK", { data: result.recordsets[0] }, res.statusCode));

      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });

  },

  get_customers:  async function (req, res) {
    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .execute("BprOps.dbo.proc_GetCustomer");
      })
      .then((result) => {
        var jsonArr = [];
        async.eachSeries(
          result.recordsets[0],
          function (obj, callback) {
            var jsonRes = {
              id: obj.Customer_ID,
              value: obj.Customer_Name,
            };
            jsonArr.push(jsonRes);
            return callback();
          },
           function (err) {
             if (err) {
               auth.setResponse(res, 400, err);
               return;
             } else {  
               return res.status(200).json(jsonArr);
             }
           }
        );

        // return res
        //       .status(200)
        //       .json(success("OK", { data: result.recordsets[0] }, res.statusCode));

      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });

  },

  get_petty_cash_exist_daily:  async function (req, res) {
    const { vehicleruteid } = req.params;

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("vehicleroute_id", sql.VarChar, vehicleruteid)
          .execute("BprOps.dbo.proc_CheckDataPettyCash_ByDaily");
      })
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));

      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });

  },

  get_petty_cash_exist_customer: async function (req, res) {
    const { customerlocation } = req.params;

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("rute_id", sql.VarChar, customerlocation)
          .execute("BprOps.dbo.proc_CheckDataPettyCash_ByRoute");
      })
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));

      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });
  },
  
  
  get_planner_customers_bydeliveryid: async function (req, res) {
    
    const { planid, driverid } = req.params;

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("plan_id", sql.VarChar, planid)
          .input("driver_id", sql.VarChar, driverid)
          .execute("BprOps.dbo.proc_Planner_GetCustomersByDeliveryPlan");
      })
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));

      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });
  },

  get_planner_soitem_bysonumber: async function (req, res) {
    const { donumber } = req.params;

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("do_number", sql.VarChar, donumber)
          .execute("BprOps.dbo.proc_Planner_GetDOItemByDONumber");
      })
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));

      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });
  },

  get_planner_accumulated_soitem_byplanid_and_customerid: async function (req, res) {
    const { ruteid, customerid } = req.params;
    //var dataArray ;
   
    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("rute_id", sql.VarChar, ruteid)
          .input("customer_id", sql.VarChar, customerid)
          .execute("BprOps.dbo.proc_Planner_GetSummaryDataSOByDeliveryPlanAndCustomerID");
      })
      .then((result) => {
        //dataArray.push(result.recordsets[0]);
        console.log('data :' , result.recordsets[0]);
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));

      })
      .catch((err) => {
        return  res.status(500).json(error(err, res.statusCode));
      });
  },

  get_planner_sonumber_byplanid_and_customerid: async function (req, res) {
    const { planid, customerid } = req.params;

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("plan_id", sql.VarChar, planid)
          .input("customer_id", sql.VarChar, customerid)
          .execute("BprOps.dbo.proc_Planner_GetSONumbersByDeliveryPlanAndCustomerID");
      })
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));

      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });
  },

  get_planner_headerid_bydriverid_deliverytoday: async function (req, res) {
    const { driverid } = req.params;

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("driver_id", sql.VarChar, driverid)
          .execute("BprOps.dbo.proc_Planner_GetPlannerIDDeliveryTodayByDriverID");
      })
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));

      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });
  },

  get_status_cico_driver: async function (req, res) {
    const { customerid, driverid } = req.params;

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("customer_id", sql.VarChar, customerid)
          .input("driver_id", sql.VarChar, driverid)
          .execute("BprOps.dbo.proc_GetDataStatusCheckInOutDriver");
      })
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: result.recordsets[0] }, res.statusCode));
      })
      .catch((err) => {
        return  res.status(500).json(error(err.originalError.info.message, res.statusCode));
      });
  },

  post_goods_receive: async function (req, res) {
    const { data } = req.body;
    //const localdate = new Date(data.timestamp);
    const clientTimeISO = new Date(data.date_isoformat);

    console.log(data);

    sql
    .connect(config)
    .then((pool) => {
         return pool
         .request()
         .input("driver_id", sql.VarChar,  data.driverid)
         .input("customer_id", sql.Float,  data.custid)
         .input("rute_id", sql.VarChar,  data.ruteid)
         .input("image_path", sql.VarChar,  data.imagePath)
         .input("local_timestamp", sql.DateTimeOffset,  clientTimeISO)
         .input("sonumbers", sql.NVarChar,  JSON.stringify(data.sonumbers))
         .input("goodssent", sql.NVarChar,  JSON.stringify(data.goodssent))
         .input("goodsreject", sql.NVarChar,  JSON.stringify(data.goodsreject))
         .execute("BprOps.dbo.proc_Planner_PostDataGR");
        })  
        .then((result) => {
          console.log(result);
          return res
                .status(200)
                .json(success("OK", { data: "OK" }, res.statusCode));
  
        })
        .catch((err) => {
          console.log(err);
          return  res.status(500).json(error(err, res.statusCode));
        });
  },

  postLocationData2: async function (req, res) {
    const { data } = req.body;
    const clientTimeISO = new Date(data.date_isoformat);

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("driver_id", sql.VarChar,  data.user_id)
          .input("location_id", sql.BigInt,  data.location_id)
          .input("location_name", sql.VarChar,  data.location_name)
          .input("location_latitude_coordinate", sql.Float,  data.location_latitude_coordinate)
          .input("location_longitude_coordinate", sql.Float,  data.location_longitude_coordinate)
          .input("clientdatetime_isoformat", sql.DateTimeOffset,  clientTimeISO)
          .input("latitude_coordinate", sql.Float,  data.coords.latitude)
          .input("longitude_coordinate", sql.Float,  data.coords.longitude)
          .input("mocked", sql.Bit,  data.mocked)
          .input("status_inout", sql.VarChar,  data.STATE_INOUT)
          .input("customerlocation_refnumber",  data.customerlocation_refnumber)
          .input("id_self_in", sql.BigInt,  data.id_self_in)
          .output("Identity", sql.BigInt)
          .execute("BprOps.dbo.proc_PostDataCheckInOutDriver");
      })  
      .then((result) => {
        //console.log(result);
        return res
              .status(200)
              .json(success("OK", { data: result.output.Identity }, res.statusCode));

      })
      .catch((err) => {
        console.log(err);
        return  res.status(500).json(error(err, res.statusCode));
      });
  },

  post_petty_cash_with_rute: async function (req, res) {
    const { data } = req.body;
    const clientTimeISO = new Date(data.date_isoformat);

    //console.log(data);

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("driver_id", sql.VarChar,  data.user_id)
          .input("rute_id", sql.VarChar,  data.rute_id)
          .input("date_iso_from_client", sql.DateTimeOffset, clientTimeISO)
          .input("parkir_price", sql.Money, data.parkir_price)
          .input("bongkar_price", sql.Money, data.bongkar_price)
          .input("checker_price", sql.Money, data.checker_price)
          .input("tarik_price", sql.Money, data.tarik_price)
          .input("other_price", sql.Money, data.other_price)
          .execute("BprOps.dbo.proc_PostDataPettyCash_ByRoute");
      })  
      .then((result) => {
        //console.log(result);
        return res
              .status(200)
              .json(success("OK", { data: "OK" }, res.statusCode));

      })
      .catch((err) => {
        console.log(err);
        return  res.status(500).json(error(err, res.statusCode));
      });
  },


  post_petty_cash_daily: async function (req, res) {
    const { data } = req.body;
    const clientTimeISO = new Date(data.date_isoformat);

    console.log(data);

    sql
      .connect(config)
      .then((pool) => {
           return pool
          .request()
          .input("driver_id", sql.VarChar,  data.user_id)
          .input("rute_id", sql.VarChar,  data.rute_id)
          .input("date_iso_from_client", sql.DateTimeOffset, clientTimeISO)
          .input("fuel_price", sql.Money, data.fuel_price)
          .input("kenek_price", sql.Money, data.kenek_price)
          .input("tol_price", sql.Money, data.tol_price)
          .execute("BprOps.dbo.proc_PostDataPettyCash_ByDaily");
      })  
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: "OK" }, res.statusCode));

      })
      .catch((err) => {
        console.log(err);
        return  res.status(500).json(error(err, res.statusCode));
      });
  },

  post_return_nonscheduled: async function (req, res) {
    const { data } = req.body;
    const clientTimeISO = new Date(data.date_isoformat);

      console.log(data.produk);

      sql
      .connect(config)
      .then((pool) => {
            return pool
          .request()
          .input("driver_id", sql.VarChar,  data.driverid)
          .input("customer_id", sql.Float,  data.customer_id)
          .input("date_iso_from_client", sql.DateTimeOffset, clientTimeISO)
          .input("return", sql.NVarChar,  JSON.stringify(data.produk))
          .execute("BprOps.dbo.proc_PostDataReturn");
      })  
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: "OK" }, res.statusCode));

      })
      .catch((err) => {
        console.log(err);
        return  res.status(500).json(error(err, res.statusCode));
      });

  },


  post_return_scheduled: async function (req, res) {
    const { data } = req.body;
    const clientTimeISO = new Date(data.date_isoformat);

    console.log(data);

      sql
      .connect(config)
      .then((pool) => {
            return pool
          .request()
          .input("driver_id", sql.VarChar,  data.user_id)
          .input("date_iso_from_client", sql.DateTimeOffset, clientTimeISO)
          .input("return", sql.NVarChar,  JSON.stringify(data.produk))
          .execute("BprOps.dbo.proc_PostDataReturnScheduled");
      })  
      .then((result) => {
        return res
              .status(200)
              .json(success("OK", { data: "OK" }, res.statusCode));

      })
      .catch((err) => {
        console.log(err);
        return  res.status(500).json(error(err, res.statusCode));
      });

  },
};

module.exports = mobileDelivery;
